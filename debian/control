Source: python-ilorest
Section: python
Priority: optional
Maintainer: Carsten Schoenert <c.schoenert@t-online.de>
Build-Depends:
 debhelper (>= 11),
 dh-python,
 python3-all,
 python3-jsonpatch,
 python3-jsonpath-rw,
 python3-setuptools,
 python3-uritools,
 python3-sphinx <!nodoc>,
 python3-sphinx-rtd-theme,
 python3-sphinxcontrib.restbuilder,
Vcs-Git: https://salsa.debian.org/tijuca/python-ilorest.git
Vcs-Browser: https://salsa.debian.org/tijuca/python-ilorest
Standards-Version: 4.4.0
Homepage: https://github.com/HewlettPackard/python-ilorest-library

Package: python-ilorest-doc
Section: doc
Architecture: all
Depends:
 python3-sphinx-rtd-theme,
 ${misc:Depends},
Description: Documentation of RESTful API for HPE iLO and HPE Chassis Manager
 This package contains the Sphinx based documentation for the library
 python-ilorest.
 .
 It contains also the examples from upstream that explain the usage of the
 library with dedicated examples.

Package: python3-ilorest
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Description: RESTful API for HPE iLO and iLO Chassis Manager based HPE servers (Python3)
 HPE RESTful API for iLO is a RESTful application programming interface for the
 management of iLO and iLO Chassis Manager based HPE servers.
 .
 REST (Representational State Transfer) is a web based software architectural
 style consisting of a set of constraints that focuses on a system's resources.
 iLO REST library performs the basic HTTP operations GET, POST, PUT, PATCH and
 DELETE on resources using the HATEOAS (Hypermedia as the Engine of Application
 State) REST architecture. The API allows the clients to manage and interact
 with iLO through a fixed URL and several URIs.
 .
 This package contains the Python 3 version.
